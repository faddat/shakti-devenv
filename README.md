# shakti-devenv

Shakti development environment that assumes a clean debian 10 installation on a server or virtual machine.  This repo is also used to automatically produce a docker image that provides the same development environment.