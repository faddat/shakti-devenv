#!/bin/bash

# Debian 10 Install Process
# This process works on a fresh Debian 10 Virtual machine.  I used the biggest instance type sold by vultr.com.
# Now testing with gitlab CI

# UPDATE / UPGRADE
apt update
apt upgrade -y

# ALL DEPENDENCIES
apt install -y ghc libghc-regex-compat-dev libghc-syb-dev libghc-split-dev libghc-old-time-dev libfontconfig1-dev libx11-dev libxft-dev flex bison tcl-dev tk-dev libfontconfig1-dev libx11-dev libxft-dev gperf iverilog tcl-itcl4-dev tk-itk4-dev git make libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python-openssl g++ autoconf automake autotools-dev curl libmpc-dev libmpfr-dev libgmp-dev libusb-1.0-0-dev gawk build-essential texinfo libtool patchutils bc zlib1g-dev device-tree-compiler pkg-config libexpat-dev python3 python3-pip

# MAKE IMPORTANT ENV VARS PERMANENT
echo 'export PATH=$PATH:/bluespec/bin' >> ~/.bashrc
echo 'export RISCV=/riscvtoolchain' >> ~/.bashrc
echo 'export PATH=$PATH:/riscvtoolchain/bin' >> ~/.bashrc
source ~/.bashrc

# BSC
git clone --recursive https://github.com/B-Lang-org/bsc
cd bsc
mkdir /bluespec
make -j $(nproc) GHCJOBS=$(nproc) GHCRTSFLAGS='+RTS -M5G -A128m -RTS' PREFIX=/bluespec # https://github.com/B-Lang-org/bsc/blob/393c8ba9537b59b39608afad513063d516d18004/.github/workflows/build.yml#L35 <- Compile like bluespec CI


# MAKE PYTHON 3 DEFAULT AND FIX PIP
update-alternatives --install /usr/bin/python python /usr/bin/python2.7 1
update-alternatives --install /usr/bin/python python /usr/bin/python3.7 2
ln -s /usr/bin/pip3 /usr/bin/pip

# VERILATOR
cd ~/
git clone https://git.veripool.org/git/verilator
cd verilator
git checkout stable
autoconf
./configure
make -j $(nproc)
make install

# RISC-V GNU TOOLCHAIN 
cd ~/
mkdir /riscvtoolchain
git clone --recursive https://github.com/riscv/riscv-opcodes # Do we do anything with this?
git clone https://github.com/riscv/riscv-gnu-toolchain # Runs slowly because of slow qemu git server
git config --file=.gitmodules quemu.Submod.url https://github.com/qemu/qemu
cd riscv-gnu-toolchain
./configure --prefix=$RISCV
make -j $(nproc)
./configure --prefix=$RISCV --with-arch=rg32vc --with-abi=ilp32d # Also, building like --prefix=$RISCV --with-arch=rg32vc --with-abi=ilp32d and then running make yeilds "nothing to be done for all."   Maybe this is an either-or?
make -j $(nproc)

# RISC-V ISA SIM
cd ~/
git clone https://gitlab.com/shaktiproject/tools/mod-spike
cd mod-spike
git checkout bump-to-latest
git clone https://github.com/riscv/riscv-isa-sim
cd riscv-isa-sim
git checkout 6d15c93fd75db322981fe58ea1db13035e0f7add
git apply ../shakti.patch
mkdir build
cd build
../configure --prefix=$RISCV
make -j $(nproc)
make install

# RISC V OPENOCD
cd ~/
git clone https://github.com/riscv/riscv-openocd --recursive
cd riscv-openocd
./bootstrap
./configure --enable-jlink --enable-remote-bitbang --enable-jtag_vpi --enable-ftdi --prefix=$RISCV
make -j $(nproc)
make install

# DEVICE TREE COMPILER
cd ~/
wget https://git.kernel.org/pub/scm/utils/dtc/dtc.git/snapshot/dtc-1.4.7.tar.gz
tar -xvzf dtc-1.4.7.tar.gz
cd dtc-1.4.7/
make NO_PYTHON=1 PREFIX=/usr/
make install NO_PYTHON=1 PREFIX=/usr/